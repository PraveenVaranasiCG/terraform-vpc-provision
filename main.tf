###########################################
#
# vpc/main.tf
#
# Praveen Varanasi
# February 2020
#
#
# Provisions VPC and needed infra inside it
#
#
###########################################

resource "aws_vpc" "vpc" {
  cidr_block = var.cidr
  tags = {
    Name = var.tag_val
  }
}


resource "aws_subnet" "public_subnet" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.public_subnet_cidr
  availability_zone       = var.public_subnet_az
  map_public_ip_on_launch = true
  tags = {
    Name = var.public_subnet_tag-val
  }
  depends_on = [aws_vpc.vpc]
}


resource "aws_subnet" "private_subnet" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = var.private_subnet_cidr
  availability_zone = var.private_subnet_az
  tags = {
    Name = var.private_subnet_tag-val
  }
  depends_on = [aws_vpc.vpc]
}


resource "aws_internet_gateway" "varanasp_igw" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = var.igw
  }
  depends_on = [aws_vpc.vpc]
}


resource "aws_route" "varanasp_rt" {
  route_table_id         = aws_vpc.vpc.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.varanasp_igw.id
  depends_on             = [aws_vpc.vpc]
}

resource "aws_default_security_group" "open_to_the_world_sg" {
  vpc_id = aws_vpc.vpc.id
  ingress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# resource "aws_route_table" "varanasp_rt" {
#   vpc_id = aws_vpc.vpc.id
#   route {
#     cidr_block = "0.0.0.0/0"
#     gateway_id = aws_internet_gateway.varanasp_igw.id
#   }
#   tags = {
#     Name = "varanasp_rt"
#   }
# }


output vpc_id {
  value = aws_vpc.vpc.id
}

output public_subnet_id {
  value = aws_subnet.public_subnet.id
}

output "private_subnet_id" {
  value = aws_subnet.private_subnet.id
}