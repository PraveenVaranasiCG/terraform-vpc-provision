
#########################################
#
# vpc/variables.tf
#
# Praveen Varanasi
# February 2020
#
#
# Contains the variables that are needed
# for aws authentication
#
#
#########################################

#vpc
variable "cidr" { default = "192.168.2.0/23" }
variable "tag_val" { default = "vpc" }

#subnets
variable "public_subnet_cidr" { default = "192.168.2.0/26" }
variable "private_subnet_cidr" { default = "192.168.3.0/27" }
variable "public_subnet_az" {  }
variable "private_subnet_az" {  }
variable "public_subnet_tag-val" { default = "public_subnet" }
variable "private_subnet_tag-val" { default = "private_subnet" }

#igw
variable "igw" { default = "igw" }
